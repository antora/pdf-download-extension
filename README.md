NOTE: the single version document uploads <manual>-<version>-version.adoc, and <manual>-<version>.pdf to PDFS.

NOTE: the multi version document downloads a number of <manual>-<version>-version.adoc and and <manual>-<version>.pdf files from PDFS

NOTE: the make-html.sh sets the correct version numbers in antora.yml and antora-playbook.yml. These files are committed, and need to be consistent for the multi version document to pick them up. The multi document converter CANNOT and does NOT run make-html.

NOTE: there seems to be still some descrepancy when asked the multi-version playbook does NOT ask for a certain version but that version is available. Just specify it, but do not include it in the non-extension part of the playbook.


To use this extensions (pdf) do the following:

- add submodules under antora/<submodule> using ```git submodule add ../../antora/pdf-download-extension.git antora/pdf-download-extension``` for the multi-version doc
```
antora:
  extensions:
    - require: ./antora/pdf-download-extension/lib/index.js
      data:
        src:
          - url: https://otp-data.web.cern.ch/pdfs
            component: "otp-user-manual"
            versions: ["1.x", "1.0"]
          - url: https://otp-data.web.cern.ch/pdfs
            component: "otp-requirements"
            versions: ["2.x", "2.0"]
```
- use a make-html.sh file like the following:
```
#!/bin/sh
set -e

antora antora-playbook.yml --stacktrace --log-level info --log-failure-level error
```

in the single version doc you need to create and upload the pdf to PDFS. Add the following to yout .gitlab-ci.yml file. These need to be separate items as they use separate images.
```
pdf-build:
  image: gitlab-registry.cern.ch/atlas-otp/otp-gitlab-ci-runner:ruby-asciidoctor
  stage: pdf-build
  needs: ["build"]
  script:
  - mkdir -p ${ATTACHMENTS}
  - ./make-pdf.sh ${CI_COMMIT_REF_NAME}
  artifacts:
    expire_in: 1 day
    paths:
    - ${SITE}

deploy-pdf:
  stage: deploy
  environment: staging
  needs: ["pdf-build"]
  script:
  - echo "adding pdf to ${CI_COMMIT_REF_NAME}"
  - echo "${KRB_PASSWORD}" | kinit ${KRB_USERNAME}@CERN.CH
  - EOS_MGM_URL=${EOSPROJECT} eos cp -r -n ${ATTACHMENTS}/${PROJECT_NAME}.pdf ${PDFS}/${PROJECT_NAME}-${CI_COMMIT_REF_NAME}.pdf

```
