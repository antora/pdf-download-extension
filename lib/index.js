'use strict'

// The name of the package in order to give the Antora logger a useful name
const { name: packageName } = require('../package.json')
const https = require('https');

const ext = '.pdf';
const attachements = 'modules/ROOT/assets/attachments/';

var logger;

module.exports.register = function ({ config }) {
    logger = this.require('@antora/logger').get(packageName)
    logger.info("registered")

    this.on('contentClassified', async function ({ contentCatalog }) {

        for (const source of config.data.src) {
            for (var version of source.versions) {
                var url = source.url + "/" + source.component + '-' + version + ".pdf";
                logger.info("downloading file " + url);
                var path = attachements + "/" + source.component + ext;
                var contents = await download(url);
                try {
                    contentCatalog.addFile({
                        contents: contents,
                        path: path,
                        src: {
                            path: path,
                            component: source.component,
                            version: version,
                            module: 'ROOT',
                            family: 'attachment',
                            relative: source.component + ext
                        },
                    });
                } catch (error) {
                    logger.warn(error)
                }
            }
        }
    })
}

function download(url) {
    return new Promise(resolve => {
        https.get(url, (res) => {
            const data = [];
            res.on('data', (chunk) => {
                data.push(chunk);
            }).on('end', () => {
                let buffer = Buffer.concat(data);
                resolve(buffer)
            });
        }).on('error', (err) => {
            logger.error('download error:', err);
        });
    });
}
